export type SiteConfig = typeof siteConfig;

export const siteConfig = {
  name: "Roman Phenko WebStudio",
  description: "Let's make beautiful websites depending on your desire.",
  navItems: [
    {
      label: "Home",
      href: "/",
    },
    {
      label: "About",
      href: "/about",
    },
    {
      label: "Portfolio",
      href: "/portfolio",
    },
    {
      label: "Blog",
      href: "/blog",
    },
    {
      label: "Pricing",
      href: "/pricing",
    },
    {
      label: "Contacts",
      href: "/contacts",
    },
  ],
  navMenuItems: [
    {
      label: "Profile",
      href: "/profile",
    },
    {
      label: "Dashboard",
      href: "/dashboard",
    },
    {
      label: "Projects",
      href: "/projects",
    },
    {
      label: "Team",
      href: "/team",
    },
    {
      label: "Calendar",
      href: "/calendar",
    },
    {
      label: "Settings",
      href: "/settings",
    },
    {
      label: "Help & Feedback",
      href: "/help-feedback",
    },
    {
      label: "Logout",
      href: "/logout",
    },
  ],
  links: {
    github: "https://github.com/Phenk0",
    gitlab: "https://gitlab.com/roman.s.parkhomenko",
    linkedin:
      "https://www.linkedin.com/in/roman-parkhomenko-6397ab235?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BM5rFITU%2BRKicysTjU4sjLg%3D%3D",
    docs: "#",
    discord: "https://discord.com/users/1035842277886672897",
    telegram: "https://t.me/roma_parkhomenko",
  },
};
