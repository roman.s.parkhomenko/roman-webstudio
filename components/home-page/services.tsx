"use client";

import { Tab, Tabs } from "@nextui-org/tabs";
import { Card, CardBody } from "@nextui-org/card";
import { motion } from "framer-motion";

import AnimatedScreenSection from "@/components/animated-screen-section";
import { subtitle, title } from "@/components/primitives";
import AccentSpan from "@/components/accent-span";

export default function Services() {
  return (
    <AnimatedScreenSection>
      <div className="flex w-full flex-col">
        <div className="py-8">
          <h1 className={title({ color: "orange", size: "lg" })}>
            Our Services
          </h1>
          <p className={subtitle()}>
            At <AccentSpan>Roman WebStudio</AccentSpan>, we offer a
            comprehensive range of services to bring your digital vision to
            life. Whether you&apos;re starting from scratch or looking to
            enhance your current online presence, we&apos;ve got you covered.
          </p>
        </div>
        <Tabs
          aria-label="Options"
          color="primary"
          placement="start"
          size="lg"
          variant="light"
        >
          <Tab key="custom-design" title="Custom Web Design">
            <Card isBlurred className="h-full">
              <CardBody className="overflow-hidden">
                <motion.div
                  animate={{ opacity: 1, scale: 1 }}
                  initial={{ opacity: 0, scale: 0.6 }}
                >
                  {" "}
                  <h3 className={title({ color: "orange", size: "sm" })}>
                    Crafting Unique Digital Experiences
                  </h3>
                  <p className={subtitle()}>
                    Our team of creative designers works closely with you to
                    create visually stunning and user-friendly websites that
                    reflect your brand&apos;s identity. From sleek corporate
                    sites to vibrant personal blogs, we ensure every design is
                    unique and tailored to your needs.
                  </p>
                </motion.div>
              </CardBody>
            </Card>
          </Tab>
          <Tab key="development" title="Web Development">
            <Card isBlurred className="h-full">
              <CardBody className="overflow-hidden">
                <motion.div
                  animate={{ opacity: 1, scale: 1 }}
                  initial={{ opacity: 0, scale: 0.6 }}
                >
                  {" "}
                  <h3 className={title({ color: "orange", size: "sm" })}>
                    Building Robust and Scalable Solutions
                  </h3>
                  <p className={subtitle()}>
                    Our expert developers bring your designs to life using the
                    latest technologies. We build fast, secure, and scalable
                    websites that provide an optimal user experience across all
                    devices. Whether you need a simple site or a complex web
                    application, we’ve got the skills to deliver.
                  </p>
                </motion.div>
              </CardBody>
            </Card>
          </Tab>
          <Tab key="e-commerce" title="E-Commerce Solutions">
            <Card isBlurred className="h-full">
              <CardBody className="overflow-hidden">
                <motion.div
                  animate={{ opacity: 1, scale: 1 }}
                  initial={{ opacity: 0, scale: 0.6 }}
                >
                  {" "}
                  <h3 className={title({ color: "orange", size: "sm" })}>
                    Empowering Your Online Store
                  </h3>
                  <p className={subtitle()}>
                    We specialize in creating powerful e-commerce platforms that
                    drive sales and enhance customer satisfaction. From
                    intuitive product listings to seamless checkout processes,
                    we design and develop e-commerce solutions that make online
                    shopping a breeze.
                  </p>
                </motion.div>
              </CardBody>
            </Card>
          </Tab>
          <Tab key="videos" title="SEO & Digital Marketing">
            <Card isBlurred className="h-full">
              <CardBody className="overflow-hidden">
                <motion.div
                  animate={{ opacity: 1, scale: 1 }}
                  initial={{ opacity: 0, scale: 0.6 }}
                >
                  {" "}
                  <h3 className={title({ color: "orange", size: "sm" })}>
                    Boosting Your Online Presence
                  </h3>
                  <p className={subtitle()}>
                    Getting noticed online is crucial. Our SEO and digital
                    marketing experts use proven strategies to improve your
                    search engine rankings and drive traffic to your site. We
                    help you reach your target audience and turn visitors into
                    loyal customers.
                  </p>
                </motion.div>
              </CardBody>
            </Card>
          </Tab>
          <Tab key="maintenance" title="Maintenance & Support">
            <Card isBlurred className="h-full">
              <CardBody className="overflow-hidden">
                <motion.div
                  animate={{ opacity: 1, scale: 1 }}
                  initial={{ opacity: 0, scale: 0.6 }}
                >
                  <h3 className={title({ color: "orange", size: "sm" })}>
                    Ensuring Smooth Operation
                  </h3>
                  <p className={subtitle()}>
                    Our job doesn&apos;t end with the launch of your website. We
                    offer ongoing maintenance and support to keep your site
                    running smoothly. From regular updates to troubleshooting
                    issues, our team is here to ensure your online presence
                    remains strong and effective.
                  </p>
                </motion.div>
              </CardBody>
            </Card>
          </Tab>
          <Tab key="content" title="Content Creation">
            <Card isBlurred className="h-full">
              <CardBody className="overflow-hidden">
                <motion.div
                  animate={{ opacity: 1, scale: 1 }}
                  initial={{ opacity: 0, scale: 0.6 }}
                >
                  <h3 className={title({ color: "orange", size: "sm" })}>
                    Engaging and Informative Content
                  </h3>
                  <p className={subtitle()}>
                    Content is king. Our team of skilled writers and content
                    creators develop engaging, informative, and SEO-friendly
                    content that resonates with your audience. From blog posts
                    to product descriptions, we provide content that adds value
                    to your website.
                  </p>
                </motion.div>
              </CardBody>
            </Card>
          </Tab>
          <Tab key="branding" title="Branding & Identity">
            <Card isBlurred className="h-full">
              <CardBody className="overflow-hidden">
                <motion.div
                  animate={{ opacity: 1, scale: 1 }}
                  initial={{ opacity: 0, scale: 0.6 }}
                >
                  <h3 className={title({ color: "orange", size: "sm" })}>
                    Creating a Cohesive Brand Image
                  </h3>
                  <p className={subtitle()}>
                    Your brand is more than just a logo; it&apos;s an
                    experience. We help you develop a strong brand identity that
                    resonates with your audience. From logo design to brand
                    guidelines, we ensure your brand stands out in the digital
                    landscape.
                  </p>
                </motion.div>
              </CardBody>
            </Card>
          </Tab>
          <Tab key="design" title="UX/UI Design">
            <Card isBlurred className="h-full">
              <CardBody className="overflow-hidden">
                <motion.div
                  animate={{ opacity: 1, scale: 1 }}
                  initial={{ opacity: 0, scale: 0.6 }}
                >
                  {" "}
                  <h3 className={title({ color: "orange", size: "sm" })}>
                    Enhancing User Experience
                  </h3>
                  <p className={subtitle()}>
                    A great website is all about the user experience. Our UX/UI
                    designers focus on creating intuitive, user-friendly
                    interfaces that make navigation a joy. We prioritize the
                    user&apos;s journey to ensure they find what they need
                    quickly and easily.
                  </p>
                </motion.div>
              </CardBody>
            </Card>
          </Tab>
        </Tabs>
      </div>
    </AnimatedScreenSection>
  );
}
