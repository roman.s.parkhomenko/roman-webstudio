"use client";

import { motion } from "framer-motion";

import { subtitle, title } from "@/components/primitives";
import AccentSpan from "@/components/accent-span";

export default function Introduction() {
  return (
    <ul
      className="py-4 h-auto md:h-screen flex flex-col justify-center items-center"
      id="introduction"
    >
      <motion.li
        className="my-4"
        exit={{ opacity: 1, scale: 1 }}
        initial={{ opacity: 0, scale: 0.5 }}
        viewport={{ once: true }}
        whileInView={{ opacity: 1, scale: [0.8, 1.3, 1] }}
      >
        <h2 className={title({ color: "orange" })}>
          There&apos;s never been a better time.
        </h2>
        <p className={subtitle()}>
          With our WebStudio, we&apos;ll help you design, develop, and launch
          the website of your <AccentSpan>dreams</AccentSpan>. Whether it&apos;s
          a personal page, an enterprise site, or a blog, we&apos;ve got you
          covered.
        </p>
      </motion.li>

      <motion.li
        className="my-4"
        exit={{ opacity: 1, scale: 1 }}
        initial={{ opacity: 0, scale: 0.5 }}
        viewport={{ once: true }}
        whileInView={{ opacity: 1, scale: [0.8, 1.3, 1] }}
      >
        <h2 className={title({ color: "orange" })}>
          Why Establish Your Presence on the Web?
        </h2>
        <p className={subtitle()}>
          In today’s digital landscape, a strong online presence is not just an
          advantage — it’s a necessity. Your website serves as your digital
          storefront, accessible 24/7 to a global audience. At{" "}
          <AccentSpan>Roman WebStudio</AccentSpan>, we recognize the
          transformative power of a well-designed website. It’s about more than
          just visibility; it’s about creating a memorable experience that
          captures your brand&apos;s essence and engages your audience. Let us
          help you build a <AccentSpan>website</AccentSpan> that not only
          represents who you are but also drives your success.
        </p>
      </motion.li>

      <motion.li
        className="my-4"
        exit={{ opacity: 1, scale: 1 }}
        initial={{ opacity: 0, scale: 0.5 }}
        viewport={{ once: true }}
        whileInView={{ opacity: 1, scale: [0.8, 1.3, 1] }}
      >
        <h2 className={title({ color: "orange" })}>
          Why Choose Our Web Studio?
        </h2>
        <p className={subtitle()}>
          Choosing the right web studio is crucial for your digital success. At{" "}
          <AccentSpan>Roman WebStudio</AccentSpan>, we believe in pushing the
          boundaries of creativity and technology. We turn your ideas into
          impactful digital experiences, ensuring your brand reaches its full
          potential. Let’s unlock the <AccentSpan>possibilities</AccentSpan>{" "}
          together and make your online presence extraordinary.
        </p>
      </motion.li>
    </ul>
  );
}
