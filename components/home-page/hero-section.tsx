"use client";

import { motion, useScroll, useTransform } from "framer-motion";
import clsx from "clsx";
import { Button } from "@nextui-org/button";
import { Link } from "@nextui-org/link";
import { button as buttonStyles } from "@nextui-org/theme";
import { Snippet } from "@nextui-org/snippet";
import NextLink from "next/link";
import Image from "next/image";

import { GithubIcon } from "@/components/icons";
import { subtitle, title } from "@/components/primitives";
import { siteConfig } from "@/config/site";

export default function HeroSection() {
  const { scrollY } = useScroll();
  const yText = useTransform(scrollY, [0, 200, 300, 500], [0, 50, 50, 100]);
  const scaleText = useTransform(scrollY, [0, 300], [1, 1.5]);
  const yCity = useTransform(scrollY, [0, 200], [0, -100]);
  const opacityCity = useTransform(scrollY, [0, 200, 300, 500], [1, 1, 1, 0]);
  const yHero = useTransform(scrollY, [0, 200], [0, -150]);
  const opacityHero = useTransform(scrollY, [0, 300, 500], [1, 1, 0]);

  return (
    <section>
      <motion.div
        className={clsx(
          "flex flex-col items-center justify-center gap-4 py-8 md:py-10 h-screen",
          "z-10 -mt-16",
        )}
        style={{ scale: scaleText, y: yText, opacity: opacityCity }}
      >
        <div className="inline-block max-w-lg text-center justify-center drop-shadow-[0_0_2px_rgba(255,255,255,0.25)] dark:drop-shadow-none">
          <h1 className={title()}>Let&apos;s make&nbsp;</h1>
          <h1 className={title({ color: "orange" })}>beautiful&nbsp;</h1>
          <br />
          <h1 className={title()}>
            website depending on your needs or desire.
          </h1>
          <h2 className={subtitle({ class: "mt-4" })}>
            Beautiful, fast and modern React/NextJS websites.
          </h2>
        </div>

        <div className="flex gap-3">
          <Button
            className="uppercase"
            color="primary"
            radius="full"
            type="button"
            variant="shadow"
          >
            <NextLink href="/#introduction">Get started</NextLink>
          </Button>
          <Link
            isExternal
            className={clsx(
              buttonStyles({ variant: "bordered", radius: "full" }),
              "text-default-700",
            )}
            href={siteConfig.links.github}
          >
            <GithubIcon size={20} />
            GitHub
          </Link>
        </div>

        <div className="mt-8">
          <Snippet hideCopyButton hideSymbol variant="flat">
            <span>
              Go straight to
              <Button
                as={NextLink}
                className="ml-2 uppercase"
                color="secondary"
                href="/portfolio"
              >
                Portfolio
              </Button>
            </span>
          </Snippet>
        </div>
      </motion.div>

      <motion.div
        className="absolute size-full top-0 left-0 -z-10"
        style={{ opacity: opacityCity, y: yCity }}
      >
        <Image
          fill
          alt="A city skyline touched by sunlight"
          className="object-cover"
          sizes="100vw"
          src="/green-city.webp"
        />
      </motion.div>

      <motion.div
        className="absolute right-1/4 drop-shadow-[0_0_6px_rgba(0,0,0,0.5)] max-w-[40%] bottom-[10%] w-80 h-1/2 2xl:right-1/4 -z-10"
        style={{ opacity: opacityHero, y: yHero }}
      >
        <Image
          fill
          alt="A superhero wearing a cape"
          className="object-contain"
          sizes="40vw, (max-width: 768px) 50vw, 40vw"
          src="/hero.png"
        />
      </motion.div>
    </section>
  );
}
