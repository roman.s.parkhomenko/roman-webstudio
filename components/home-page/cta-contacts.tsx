import { Link } from "@nextui-org/link";

import { subtitle, title } from "@/components/primitives";
import AccentSpan from "@/components/accent-span";
import AnimatedScreenSection from "@/components/animated-screen-section";

export default function CtaContacts() {
  return (
    <AnimatedScreenSection id="contacts">
      <div>
        <h2
          className={title({
            color: "orange",
            fullWidth: true,
            className: "mb-4",
          })}
        >
          Ready to bring your vision to life? Contact us today!
        </h2>
        <p className={subtitle()}>
          We combine creativity with <AccentSpan>cutting-edge</AccentSpan>{" "}
          technology to deliver stunning and <AccentSpan>effective</AccentSpan>{" "}
          websites.
        </p>
        <p className={subtitle()}>
          Have questions? Reach out to us at{" "}
          <Link
            isBlock
            href="mailto:roman.s.parkhomenko@gmail.com"
            inputMode="email"
            size="lg"
          >
            <AccentSpan>roman.s.parkhomenko@gmail.com</AccentSpan>
          </Link>{" "}
          or call us at{" "}
          <Link isBlock href="tel:+380997434075" size="lg">
            <AccentSpan>+38(099) 743-4075</AccentSpan>
          </Link>
          .
        </p>
      </div>
    </AnimatedScreenSection>
  );
}
