import { title } from "@/components/primitives";
import AnimatedScreenSection from "@/components/animated-screen-section";
import FancyTestimonialsSlider from "@/components/fancy-testimonial-slider";
import Constructing from "@/public/constructing.png";
import PlayingGuitar from "@/public/playing-guitar.png";
import Geek from "@/public/working-on-computer.png";

//todo: change images
const testimonials = [
  {
    img: Constructing,
    quote:
      "Roman WebStudio exceeded our expectations! The team was incredibly professional, responsive, and creative. Our new website has received fantastic feedback, and our traffic has increased significantly. Highly recommend their services!",
    name: "Inna K.",
    role: "Dean, FDO GNPU",
  },
  {
    img: PlayingGuitar,
    quote:
      "From start to finish, Roman WebStudio was amazing. They designed a sleek and user-friendly website that perfectly represents our Institute. Thank you, WebStudio!",
    name: "Andrii S.",
    role: "Director, NNIPP GNPU",
  },
  {
    img: Geek,
    quote:
      "The team at Roman WebStudio is top-notch. They took the time to understand my business and created a website that is both beautiful and functional. Their attention to detail and commitment to quality are unparalleled. I’m thrilled with my new site!",
    name: "John D.",
    role: "CEO, Dolphinarium",
  },
  {
    img: Constructing,
    quote:
      "Roman WebStudio transformed my online presence. The site they built for me is visually appealing, easy to navigate, and perfectly aligned with my brand. Their support throughout the process was outstanding. I highly recommend them to anyone looking to enhance their online presence.",
    name: "Jane D.",
    role: "Blogger",
  },
];

export default function Testimonials() {
  return (
    <AnimatedScreenSection>
      <h2 className={title({ color: "orange", className: "mb-6" })}>
        Our clients love what we do.
      </h2>
      <FancyTestimonialsSlider testimonials={testimonials} />
    </AnimatedScreenSection>
  );
}
