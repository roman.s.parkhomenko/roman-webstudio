"use client";

import NextLink from "next/link";
import { usePathname } from "next/navigation";
import clsx from "clsx";
import { link as linkStyles } from "@nextui-org/theme";

interface NavLinkProps {
  href: string;
  children: React.ReactNode;
}

export default function NavLink({ href, children }: NavLinkProps) {
  const path = usePathname();
  const isActive =
    href === "/" || href.startsWith("/#")
      ? path === href
      : path?.startsWith(href);

  return (
    <NextLink
      className={clsx(
        linkStyles({ color: "foreground" }),
        isActive ? "text-primary font-bold" : undefined,
      )}
      color="foreground"
      href={href}
    >
      {children}
    </NextLink>
  );
}
