export default function AccentSpan({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <>
      <span className="bg-clip-text text-transparent bg-gradient-to-b from-[#C4661F] to-[#783d19] dark:from-[#ffd7b9] dark:to-[#ffb66e] font-bold">
        {children}
      </span>
    </>
  );
}
