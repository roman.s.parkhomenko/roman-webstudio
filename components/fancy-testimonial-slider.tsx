"use client";

import { useState, useRef, useEffect } from "react";
import Image, { StaticImageData } from "next/image";
import { AnimatePresence, motion } from "framer-motion";
import { Button } from "@nextui-org/button";

import { subtitle } from "@/components/primitives";

interface Testimonial {
  img: StaticImageData;
  quote: string;
  name: string;
  role: string;
}

export default function FancyTestimonialsSlider({
  testimonials,
}: {
  testimonials: Testimonial[];
}) {
  const testimonialsRef = useRef<HTMLDivElement>(null);
  const [active, setActive] = useState<number>(0);
  const [autorotate, setAutorotate] = useState<boolean>(true);
  const autorotateTiming: number = 7000;

  useEffect(() => {
    if (!autorotate) return;
    const interval = setInterval(() => {
      setActive(
        active + 1 === testimonials.length ? 0 : (active) => active + 1,
      );
    }, autorotateTiming);

    return () => clearInterval(interval);
  }, [active, autorotate]);

  const heightFix = () => {
    if (testimonialsRef.current && testimonialsRef.current.parentElement)
      testimonialsRef.current.parentElement.style.height = `${testimonialsRef.current.clientHeight}px`;
  };

  useEffect(() => {
    heightFix();
  }, []);

  return (
    <div className="mx-auto w-full max-w-3xl text-center">
      {/* Testimonial image */}
      <div className="relative h-32">
        <div className="pointer-events-none absolute before:absolute before:inset-0 top-0 left-1/2 before:-z-10 -translate-x-1/2 before:rounded-full before:bg-gradient-to-b before:from-amber-500/25 before:via-amber-500/5 before:to-amber-500/0 w-[480px] h-[480px] before:via-25% before:to-75%">
          <div className="h-32 [mask-image:_linear-gradient(0deg,transparent,theme(colors.white)_20%,theme(colors.white))]">
            <AnimatePresence>
              {testimonials.map(
                (testimonial, index) =>
                  active === index && (
                    <motion.div
                      key={index}
                      animate={{ opacity: 1, rotate: 0 }}
                      className="absolute inset-0 -z-10 h-full"
                      exit={{ opacity: 0, rotate: 60 }}
                      initial={{ opacity: 0, rotate: -60 }}
                      transition={{
                        duration: 0.7,
                        ease: [0.68, -0.3, 0.32, 1],
                      }}
                    >
                      <Image
                        alt={testimonial.name}
                        className="relative top-11 left-1/2 -translate-x-1/2 rounded-full"
                        height={56}
                        src={testimonial.img}
                        width={56}
                      />
                    </motion.div>
                  ),
              )}
            </AnimatePresence>
          </div>
        </div>
      </div>
      {/* Text */}
      <div className="mb-9 transition-all delay-300 duration-150 ease-in-out">
        <div className="relative flex flex-col">
          <AnimatePresence>
            {testimonials.map(
              (testimonial, index) =>
                active === index && (
                  <motion.div
                    key={index}
                    ref={testimonialsRef}
                    animate={{ opacity: 1, translateX: 0 }}
                    className="absolute"
                    exit={{ opacity: 0, translateX: 16 }}
                    initial={{ opacity: 0, translateX: -16 }}
                    transition={{
                      duration: 0.5,
                      ease: "easeInOut",
                      delay: 0.2,
                    }}
                    onAnimationStart={() => heightFix()}
                  >
                    <div className={subtitle()}>{testimonial.quote}</div>
                  </motion.div>
                ),
            )}
          </AnimatePresence>
        </div>
      </div>
      {/* Buttons */}
      <div className="flex flex-wrap justify-center -m-1.5">
        {testimonials.map((testimonial, index) => (
          <Button
            key={index}
            className="m-1.5"
            color={active === index ? "primary" : "default"}
            size="sm"
            variant={active === index ? "solid" : "flat"}
            onClick={() => {
              setActive(index);
              setAutorotate(false);
            }}
          >
            <span>{testimonial.name}</span> - <span>{testimonial.role}</span>
          </Button>
        ))}
      </div>
    </div>
  );
}
