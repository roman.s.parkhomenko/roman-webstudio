"use client";

import { motion } from "framer-motion";

interface AnimatedScreenSectionProps {
  id?: string;
  children: React.ReactNode;
}

export default function AnimatedScreenSection({
  id,
  children,
}: AnimatedScreenSectionProps) {
  return (
    <motion.section
      className="flex min-h-screen w-full flex-col items-center justify-center overflow-hidden py-12"
      id={id}
      initial={{ opacity: 0, scale: 0.7, x: -100 }}
      viewport={{ amount: 0.2 }}
      whileInView={{ opacity: 1, scale: 1, x: 0 }}
    >
      {children}
    </motion.section>
  );
}
