import { nextui } from "@nextui-org/theme";

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
    "./node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: ["var(--font-sans)"],
        mono: ["var(--font-geist-mono)"],
      },
    },
  },
  darkMode: "class",
  plugins: [
    nextui({
      themes: {
        dark: {
          colors: {
            primary: {
              DEFAULT: "#ffb66e",
              foreground: "#222",
            },
            secondary: {
              DEFAULT: "#A9b388",
              foreground: "#222222",
            },
            focus: "#bb7735",
            background: { DEFAULT: "#5f6f52" },
          },
        },
        light: {
          colors: {
            primary: {
              DEFAULT: "#c4661f",
              foreground: "#ffffff",
            },
            secondary: {
              DEFAULT: "#5f6f52",
              foreground: "#eeeeee",
            },
            focus: "#c4661f",
            background: { DEFAULT: "#fefae0" },
          },
        },
      },
    }),
  ],
};
