import HeroSection from "@/components/home-page/hero-section";
import Introduction from "@/components/home-page/introduction";
import Services from "@/components/home-page/services";
import Testimonials from "@/components/home-page/testimonials";
import { subtitle } from "@/components/primitives";
import CtaContacts from "@/components/home-page/cta-contacts";

export default function Home() {
  return (
    <>
      <HeroSection />
      <Introduction />
      <Services />
      <Testimonials />

      {/*todo: add visuals(website showcases)*/}

      <CtaContacts />
    </>
  );
}
