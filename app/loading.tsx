import { Spinner } from "@nextui-org/spinner";

export default function Loading() {
  return (
    <div className="flex h-screen flex-col items-center justify-center -m-40">
      <Spinner label="Loading..." size="lg" />
    </div>
  );
}
