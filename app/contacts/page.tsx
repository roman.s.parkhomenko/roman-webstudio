import AnimatedScreenSection from "@/components/animated-screen-section";
import { subtitle, title } from "@/components/primitives";
import AccentSpan from "@/components/accent-span";

export default function ContactsPage() {
  return (
    <AnimatedScreenSection id="contacts">
      <header className="my-8 w-full">
        <h2 className={title({ color: "orange" })}>
          Ready to Bring Your Vision to Life?
        </h2>
        <p className={subtitle()}>
          At WebStudio, we blend creativity with cutting-edge technology to
          deliver stunning and effective websites. Whether you’re a startup, an
          established business, or an individual with a vision, we’re here to
          turn your ideas into reality.
        </p>
      </header>
      <section className="my-8 w-full">
        <h2 className={title({ color: "orange" })}>Choose Us</h2>
        <p className={subtitle()}>
          <AccentSpan>Innovative Designs:</AccentSpan> Our team crafts unique
          and visually captivating designs tailored to your brand.
        </p>
        <p className={subtitle()}>
          <AccentSpan>Technical Expertise:</AccentSpan> We use the latest
          technologies to build robust and scalable websites.
        </p>
        <p className={subtitle()}>
          <AccentSpan>Comprehensive Solutions:</AccentSpan> From design and
          development to SEO and maintenance, we offer end-to-end services.
        </p>
        <p className={subtitle()}>
          <AccentSpan> Client-Centric Approach:</AccentSpan> Your satisfaction
          is our priority. We work closely with you to ensure your website meets
          your goals.
        </p>
      </section>
      <footer className="my-8 w-full">
        <h2 className={title({ color: "orange" })}> Get in Touch</h2>
        <p className={subtitle()}>
          Have questions or ready to start your project? We’d love to hear from
          you!
        </p>
        <p className={subtitle()}>
          📧 Email: roman.s.parkhomenko@gmail.com <br />
          📞 Phone: +38 (099) 743-4075
        </p>
      </footer>
    </AnimatedScreenSection>
  );
}
